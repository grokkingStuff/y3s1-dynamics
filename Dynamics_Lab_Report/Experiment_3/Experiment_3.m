% |===================================|
% | Vi Kumar <grokkingstuff@gmail.com |
% | 2020-11-27                        |
% |===================================|


%% System Definition
% Do describe the system here (write the diffe equation)
% Assuming no damping (odd). Maybe check if you need damping?
% Also, do add units to your things!

% Both positions are NOT at zero and have zero velocity
initial_x = [100;  % = position y1 at t = 0
             0]; % = velocity y1_dot at t = 0

% Matrix Notation
% ===============

% x1 - some column vector

% x(1) = position x1
% x(2) = velocity x1_dot

% Dx(1) = velocity     x1_dot
% Dx(2) = acceleration x1_dotdot
            
         
% System Equation         
% ===============

% mx'' + cx' + kx = 0
% mx'' = -cx' + -kx
% x'' = (-c/m)x' + (-k/m)x

m = 1212.5e-3; % Masses
c = 8;  % Damping Constant
k = 8000;  % Spring Constant

% System Descriptors
% ==================
omega = sqrt(k/m);                  % Natural frequency
p = c/(2*m);                        % Damping Ratio
omega_damped = omega*sqrt(1 - p^2); % "Real" frequency


% X_dot & X are related by matrix A
A = [0,      1;
     -(k/m), (-c/m)];

% Write the answer for yourself in the future, dummy. 
% Nothing is obvious
 
Dx = @(t,x) A*x;
% Don't bother writing helper function if you keep having to hide
%     them at the bottom of a script, not for something trivial


%% Integration & a brief explanation of ode45

% Let's run the sim for about 5 seconds. 

%Repeat god knows how many times.
%x = x + Dx*delta_t

% ==============================================
[t,x] = ode45(Dx, ...      % Dx = A*x         
              [0 5], ...   % t = 0 -> t = 5
              initial_x);  % x(t = 0)
% ==============================================
 
% What is t & x? And how did I get this?
% ode45 requires three arguments
%   1 a matrix equation relating Dx to your time t & your system vector x
%   2 some time range
%   3 and an initial condition
% 
% So ODE45 is AMAZING at being beginner-friendly
% It does it numerically but you don't need to ...
%     worry about convergence stuff. 
% It figures out what the appropriate time step ...
%     size is automatically.
% The t output is the time index. If you look at it, ...
%     you'll see the step size be somewhat tiny
% The y output is every single y' at those times t
% So at each step, take y, transpose it, and add it to the output.
% Wonderful in this case.
%
% Keep in mind that ode45 does not respect conservation of
%     energy AND all numerical methods have floating point errors.
%     Extend the thing longer and you'll start seeing errors.
% Totally unnecessary to do 15 seconds since ...
%     this system has NO damping.
% If you must use this particular case and without damping, ...
% just replicate the array

%% Plotting          

% Gonna plot the positions only
plot(t,x(:,1))

ylim([-150,150]);
title("Position of points wrt time")
xlabel("Time (seconds)")
ylabel("Position (???)")
