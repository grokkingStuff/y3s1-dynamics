
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Apparatus
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Apparatus}

%In our apparatus, external work is applied to the system by Rotor Excitation (the motor with the set of unbalanced rotating disc). In our apparatus, the external force exerted is given by equation.

%\begin{equation}
%F_{ext} = me{\omega^2}b\sin{\omega t}
%\label{eqn:external_force}
%\end{equation}

\begin{figure*}
	\caption{Apparatus}
	\label{fig:springmassdamper}
	\includegraphics{Mass_spring_damper.png}
\end{figure*}






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Equipment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Equipment}
The apparatus consists of a vibrating spring-mass-damper system driven by an electric motor supplied with variable frequency AC current. \sidenote{
Equipment List: 
\begin{itemize}
	\item Beam rig with safety cover panel
	\item Power supply
	\item Frequency Controller
	\item Steel Beam
	\item Damper
	\item Motor
	\item 2 Aluminum discs
	\item Disc belt
	\item Spring
	\item LVDT
	\item Photolight Sensor
	\item Computer with LabView
\end{itemize}
}
The vibrating beam is supported by a pivot at one end and by a coiled spring at the other end. A viscous liquid damper is also attached to the middle of the beam to dissipate energy.

The vibration is mainly caused by out-of-balance masses on the rotating disc. Both motor \& rotating disc are rigidly attached to the beam so that the beams vibrates in the vertical direction about its pivot. 

The vertical oscillation of the beam is measured by a transducer and the rotational rate of the unbalanced disc is measured by photosensor.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Relevant Terms & Quantities
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{margintable}
\caption{Relevant Terms \& Quantities}
\begin{tabular}{ll}
\toprule
Symbol & Quantity \\
\midrule
$b$ & 350mm \\
$L$ & 830mm \\
$d$ & 640mm \\
$a$ & 155mm \\
$f$ & 760mm \\
\midrule
$M_{drive}$ & 2kg \\
$b_{t}$ & 25mm \\
$b_{w}$ & 12mm \\
\bottomrule
\end{tabular}
\end{margintable}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Relationship between Linear Displacement & Rotational terms in the Apparatus
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{margintable}
\caption{Relationship between Linear Displacement \& Rotational terms in the Apparatus}
\begin{tabular}{rl}
	\toprule
	Linear & Rotational \\
	\midrule
	$x$ & = $d\theta$ \\
	$h_{c}$ & = $b\theta$ \\
	$h_{f}$ & = $f\theta$ \\
	$h_{b}$ & = $\frac{L}{2}\theta$ \\ 
	$\dot{h_{c}}$ & = $b\dot{\theta}$ \\
	$\dot{h_{f}}$ & = $f\dot{\theta}$ \\
	$\dot{h_{b}}$ & = $\frac{L}{2}\dot{\theta}$ \\
	$v_{damper}$ & = $c\dot{\theta}$ \\
	\bottomrule
\end{tabular}
\end{margintable}
























%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Energy Terms \& Conservation Equations}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We know that from the conservation for energy law, the sum of all energies in a vibrating system is constant if no external force acts on the system. \sidecite{Hibbeler2017}.
$$ {KE}_{1} + {PE}_{1} = {KE}_{2} + {PE}_{2} $$ 
We can broadly classify energy into Kinetic \& Potential Energy. There are two types of Kinetic energies, translational and rotational, there are also two types of potential energies, gravitational and elastic.

%% Relevant Energy Terms in the Apparatus
\begin{table*}
\caption{Relevant Energy Terms in the Apparatus}
\begin{tabular}{rl}
	\toprule \\
	Energy & Formula \\
	\midrule \\
	Kinetic Energy of point mass $m_{b}$   & $\frac{m_{c}v_{c}^{2}}{2}$ \\
	Kinetic Energy of point mass $m_{f}$   & $\frac{m_{f}v_{f}^{2}}{2}$ \\
	Kinetic Energy for rotating rigid mass & $\frac{I \omega_{1}^{2} }{2}$ \\
	\midrule \\
	Potential Spring Energy                   & $\frac{kv_{1}^{2}}{2}$	\\
	Potential Gravitational Energy of $m_{b}$ & $ mgh_{b} $	\\
	Potential Gravitational Energy of $m_{f}$ & $ mgh_{f} $	\\
	\bottomrule 
\end{tabular}
\end{table*}

\begin{equation}
	\frac{1}{2}mv^{2} + \frac{1}{2}I\dot{\theta}^{2} + mgh + \frac{1}{2}kx^{2} = const 
\end{equation}

%% Relevant Inertia Terms in the Apparatus
\begin{margintable}
\caption{Relevant Inertia Terms in the Apparatus}
\begin{tabular}{rl}
	$I_{b}$ & = $m_{b}\frac{L^2}{3}$ \\
	$I_{ass}$ & = $m_{b}\frac{L^2}{3} + m_{c}b^2$ \\
	$I_{o}$ & = $I_{ass} + m_{f}f^2$ \\
\end{tabular}
\end{margintable}

\begin{equation*}
	\frac{1}{2} m_{c} {b\dot{\theta}}^{2} + \frac{1}{2} m_{f} {f\dot{\theta}}^{2} +	\frac{1}{2} I_{b} {\dot{\theta}}^{2} + \frac{1}{2}k{d\theta}^{2} = const 
\end{equation*}

\begin{equation*}
	m_{c} {b^2} \ddot{\theta}\dot{\theta} + m_{f} {f^2} \ddot{\theta}\dot{\theta} +	I_{b} \ddot{\theta}\dot{\theta} + k{d^2}\dot{\theta}\theta = 0 
\end{equation*}

When we remove and eliminate common terms ($\dot{\theta}$, and state $I_{ass} = ( m_{c} {b^2} + m_{f} {f^2} + I_{b} ) $, we get:

\begin{equation*}
	I_{ass}\ddot{\theta} + k{d^2}\theta = 0 
\end{equation*}

When we include the Damping \& External Forces, our system equations becomes:

\begin{equation}
	\label{eqn:sys_equation}
	I_{ass}\ddot{\theta} + c{a^2}\dot{\theta} + k{d^2}\theta = F_{ext}
\end{equation}


\begin{kaobox}[frametitle=Summary of procedure for calculating natural frequencies]
	\begin{enumerate}
	\item Describe the motion of the system, using a single scalar variable (linear displacement $s$ or rotational displacement $\theta$); \marginnote{
		In a spring-mass-damper system, we can take the datum point to be some point of the system at equilibrium. At equilibrium, the initial kinetic energy is 0, making it an ideal reference point for potential energy calculations. (and hence the total enerrgy).
		}
	\item Write down the potential energy ($V$) and kinetic energy ($T$) of the system in terms of the scalar variable;
	\item Use differenciation wrt time ($\ddot{T}+\ddot{V}=0$) to get an equation of motion for your scalar variable;
	\end{enumerate}
\end{kaobox}
