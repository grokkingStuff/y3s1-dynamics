%% Setup the Import Options and import the data
opts = delimitedTextImportOptions("NumVariables", 9, "Encoding", "UTF-8");
opts.DataLines = [3, Inf];
opts.Delimiter = ",";
opts.VariableNames = ["t", "LVDT1", "photo1", ...	
                      "t_2", "LVDT2", "photo2", ...	
                      "t_3", "LVDT3", "photo3"];
opts.VariableTypes = ["double", "double", "double", ...
                      "double", "double", "double", ...
                      "double", "double", "double"];
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
EXP1 = readtable(".\EXP1_data.csv", opts);
clear opts

%% Detrend data
data = table2array(EXP1);
data = data(:,[2,3,5,6,8,9]);
data = smoothdata(data,1);
data = data - mean(rmmissing(data));
%% Assign to timetable
data = array2timetable(data,'SampleRate',(1/2.5e-4));
data.Properties.VariableNames = ["LVDT1","photo1","LVDT2","photo2","LVDT3", "photo3"];
data = rmmissing(data);
head(data)

%% FFT Analysis
[p1,f1] = pspectrum(data(:,"LVDT1"));
[p2,f2] = pspectrum(data(:,"LVDT2"));
%[p2,f2] = pspectrum(data(timerange(seconds(0.2),seconds(0.4)),"LVDT2"));
[p3,f3] = pspectrum(data(:,"LVDT3"));

% Ignoring DC components
p1(f1<0.1) = 0;
p2(f2<0.1) = 0;
p3(f3<0.1) = 0;
[~,index_101] = max(p1);
[~,index_301] = max(p2);
[~,index_1212] = max(p3);
mass = [101.5, 301.5, 1212.5];
mass = mass / 1e3;
frequency = [f1(index_101),...
             f2(index_301), ...
             f3(index_1212)];
time_period = 1./frequency;         

         
%% Finding system natural frequency

%b = 350mm 
%L = 830mm
%d = 640mm
%M_drive = 2kg
%thickness 25mm
%width = 12mm
%a = 155mm
%f = 760mm
         
syms f d K m_f I_ass T_n        
term1 = (4*sym(pi)^2/(K*d*d));
eq = T_n^2 - term1*(I_ass + m_f);

eq = subs(eq,f,0.760);
eq = subs(eq,d,0.640);

eq1 = subs(subs(eq,m_f, mass(1)),T_n, time_period(1));
eq2 = subs(subs(eq,m_f, mass(3)),T_n, time_period(3));

eqn = [eq1 == 0, eq2 == 0];
S = solve(eqn,[K, I_ass]);
% Plot Solutions
disp("K")
double(S.K)
disp("I_ass")
double(S.I_ass)













%% PLOTTING BULLSHIT
 
figure;
 
%% Plot the FFT spectrums on left:
subplot(4,2,1); hold on; plot(f1,pow2db(p1)); grid on; hold off;
xlabel('Frequency (in hertz)'); ylabel('Amplitude');
title('FFT 101.5');

subplot(4,2,3); hold on; plot(f2,pow2db(p2)); grid on; hold off;
xlabel('Frequency (in hertz)'); ylabel('Amplitude');
title('FFT 301.5');

subplot(4,2,5); hold on; plot(f3,pow2db(p3)); grid on; hold off;
xlabel('Frequency (in hertz)'); ylabel('Amplitude');
title('FFT 1212.5');

%% Plot the t series on the right
subplot(4,2,2); hold on;
plot(data.Time,data.LVDT1,'-');hold off;
%plot(data(:,"LVDT1")); plot(data(:,"photo1")); hold off;
xlabel('Time (in seconds)'); ylabel('Amplitude'); 
title('Response for 101.5 g');

subplot(4,2,4); hold on; 
plot(data.Time,data.LVDT2,'-');hold off;
%plot(data(:,"LVDT2")); plot(data(:,"photo2")); hold off;
xlabel('Time (in seconds)'); ylabel('Amplitude'); 
title('Response for 301.5 g');

subplot(4,2,6); hold on; 
plot(data.Time,data.LVDT3,'-');hold off;
%plot(data(:,"LVDT3")); plot(data(:,"photo3")); hold off;
xlabel('Time (in seconds)'); ylabel('Amplitude'); 
title('Response for 1212.5 g');

%% Plot T_n vs m_f
subplot(4,2,[7,8]);  
cnames = {'Mass','Time Period',};
rnames = {'Mass 1','Mass 2','Mass 3'};
T = table(mass', time_period','RowNames',rnames);


% Create the uitable
t = uitable('Data',T{:,:},'ColumnName',T.Properties.VariableNames,...
    'RowName',T.Properties.RowNames,'ColumnName',cnames,'Units', 'Normalized', 'Position',[0, 0, 1, 1]);

subplot(4,2,[7,8]),plot(3)
pos = get(subplot(4,2,[7,8]),'position');
delete(subplot(4,2,[7,8]))
set(t,'units','normalized')
set(t,'position',pos)



% 
% 

% 
% %% HELPER FUNCTIONS
% function [photo] = clean_photo(data)
% 
% 
% 
% end



% 
% data = table2array(EXP1);
% data = rmmissing(data);
% 
% 
% %% Initial data cleaning, time series & frequency specification
% photo = {}; lvdt = {};
% photo.data = 5 - [data(:,3)';data(:,3+3)';data(:,3+6)'];
% lvdt.data = [data(:,2)'; data(:,2+3)'; data(:,2+6)'];
% photo.data(photo.data < 1) = 0 ;
% 
% photo.N = length(photo.data(1,:));               % Length of signal
% photo.n = 2^nextpow2(photo.N);                   % Next highest power of 2
% photo.Ts = 2.5e-4;                               % Sample Time
% photo.t = linspace(0,photo.N*photo.Ts,photo.N);  % time vector
% photo.Fs = 1/photo.Ts;                          % Sampling frequency
% 
% lvdt.N = length(lvdt.data(1,:));             % Length of signal
% lvdt.n = 2^nextpow2(lvdt.N);                 % Next highest power of 2
% lvdt.Ts = 2.5e-4;                            % Sample Time
% lvdt.t = linspace(0,lvdt.N*lvdt.Ts,lvdt.N);  % time vector
% lvdt.Fs = 1/lvdt.Ts;                         % Sampling frequency
% 
% 
% 
% 
% %% FFT Stuff for Photo
% N = photo.N;
% n = photo.n;
% Fs = photo.Fs;
% 
% photo.fft = fft(photo.data,n,2);
% P2 = abs(photo.fft/N);
% P1 = P2(:,1:n/2+1);
% P1(:,2:end-1) = 2*P1(:,2:end-1);
% 
% f = 0:(Fs/n):(Fs/2-Fs/n);
% photo.f = f;
% photo.a = P1(:,1:n/2);
% 
% %% FFT Stuff for LVDT
% N = lvdt.N;
% n = lvdt.n;
% Fs = lvdt.Fs;
% 
% lvdt.fft = fft(lvdt.data,n,2);
% P2 = abs(lvdt.fft/N);
% P1 = P2(:,1:n/2+1);
% P1(:,2:end-1) = 2*P1(:,2:end-1);
% 
% f = 0:(Fs/n):(Fs/2-Fs/n);
% lvdt.f = f;
% lvdt.a = P1(:,1:n/2);