% Solution of Nonlinear Pendulum Problem
% Coded by I. Ashayeri 


clc;            % This clears the command window
clear all;      % This clears the workspace of any variables
close all;      % This closes any other MATLAB windows that may be open

% Initialising variables
global omega_squared
m=0.5;
g=9.81;
l=0.9;
omega_squared=-(m*g)/(m*l);
x_range=[0:0.01:5];
xo=[pi/3,0];        % Initial conditions

[t,x]=ode45('rhsode',x_range,xo);       % Solving the ODE

angle=x(:,1);
% Plotting the output
plot(t,angle,'k--','Linewidth',1.5);
xlabel('Time(t)'),ylabel('Angular Motion, \theta (rad)');
grid on;
title('Numerical Solution of d^2\theta/dt^2+\omega^2sin\theta=0');
axis([0,5,-1.5,1.5]);
set(gca,'fontsize',20);





