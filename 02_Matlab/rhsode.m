function dxdt=rhsode(t,x)
global omega_squared;
dxdt=[x(2);omega_squared*sin(x(1))];

end